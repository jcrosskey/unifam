#!/bin/bash

#PBS -N GuestGenome
#PBS -q large
#PBS -l walltime=360:00:00
#PBS -l nodes=1:ppn=2
###############################
### Display the job context ###
###############################
WORKDIR=/chongle/jj/01_PfClust/CodeBase/example/GuestGenome
echo Working directory is $WORKDIR
cd $WORKDIR

echo Running on host `hostname`
echo Starting Time is `date`
echo Directory is `pwd`

python /home/cjg/tools/UniFam/src/UniFam.py -i GuestGenome.fna -c  config.cfg  > GuestGenome.log

echo Ending Time is `date`
